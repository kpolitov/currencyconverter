package net.jesseshores.currencyconverter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

class NetworkUtils {

    private static final String TAG = NetworkUtils.class.toString();
    private static final int BUFFER_SIZE = 8192;

    interface Callback {
        void call(boolean result);
    }

    private Callback m_callback;

    private AsyncTask<String, Void, Boolean> m_downloadTask = null;

    void downloadFile(String location, String destination, Callback callback) {
        stopPendingDownload();
        m_downloadTask = new AsyncTask<String, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(String [] params) {
                boolean result = false;
                try {
                    String location = params[0];
                    String destination = params[1];
                    URL url = new URL(location);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    try (InputStream input = new BufferedInputStream(url.openStream(), BUFFER_SIZE);
                         OutputStream output = new FileOutputStream(destination)) {
                        int count = 0;
                        byte[] buffer = new byte[BUFFER_SIZE];
                        while ((count = input.read(buffer)) != -1) {
                            output.write(buffer, 0, count);
                        }
                    }
                    result = true;
                }
                catch (IOException e) {
                    Log.e(TAG, "Failed to download file", e);
                }
                return result;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                m_callback.call(result);
            }
        };
        m_downloadTask.execute(location, destination);
        m_callback = callback;
    };

    void stopPendingDownload() {
        if (m_downloadTask != null) {
            m_downloadTask.cancel(true);
            m_downloadTask = null;
        }
    };
}
