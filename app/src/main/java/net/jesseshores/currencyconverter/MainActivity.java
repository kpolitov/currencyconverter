package net.jesseshores.currencyconverter;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import net.jesseshores.currencyconverter.CurrencyUtils.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends Activity {

    private final Model m_model = new Model();
    private Observable m_updateObservable = null;
    private ExchangeRates m_rates = new ExchangeRates();

    private Observer m_updateObserver = new Observer() {
        @Override
        public void update(Observable o, Object arg) {
            if ((Boolean)arg) {
                setExchangeRates(m_model.getExchangeRates(MainActivity.this));
                updateInfoField(InfoType.INFO, getString(R.string.cache_valid) + m_rates.getDate());
            }
            else {
                if (!m_rates.getDate().isEmpty()) {
                    updateInfoField(InfoType.WARNING, getString(R.string.download_failed_cache_valid) + m_rates.getDate());
                }
                else {
                    updateInfoField(InfoType.ERROR, getString(R.string.download_failed_cache_invalid));
                    findViewById(R.id.etValue).setEnabled(false);
                }
            }
        }
    };

    private void updateFields() {
        List<String> charCodeList = new ArrayList<>(m_rates.getCurrencyList().size());
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, charCodeList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        int defaultCodeFrom = 0, defaultCodeTo = 0;
        for (int i=0; i<m_rates.getCurrencyList().size(); i++) {
            Currency item = m_rates.getCurrencyList().get(i);
            charCodeList.add(item.getCharCode() + " (" + item.getName() + ")");
            if ("RUR".equals(item.getCharCode())) {
                defaultCodeFrom = i;
            } else if ("USD".equals(item.getCharCode())) {
                defaultCodeTo = i;
            }
        }
        Spinner spTo = (Spinner) findViewById(R.id.spTo);
        Spinner spFrom = (Spinner) findViewById(R.id.spFrom);
        spTo.setAdapter(spinnerAdapter);
        spFrom.setAdapter(spinnerAdapter);
        spTo.setSelection(defaultCodeTo);
        spFrom.setSelection(defaultCodeFrom);
        updateExchangeResult();
    };

    enum InfoType {
        INFO,
        WARNING,
        ERROR
    }

    private void setExchangeRates(ExchangeRates exchangeRates) {
        m_rates = exchangeRates;
        updateFields();
    }

    private void updateInfoField(InfoType type, String info) {
        TextView tvInfo = (TextView) findViewById(R.id.tvInfo);
        tvInfo.setText(info);
        int color = getColor(R.color.colorInfo);
        switch (type) {
            case WARNING: {
                color = getColor(R.color.colorWarning);
                break;
            }
            case ERROR: {
                color = getColor(R.color.colorError);
                break;
            }
        }
        tvInfo.setTextColor(color);
    }

    private void updateExchangeResult() {
        StringBuilder builder = new StringBuilder();
        final EditText etValue = (EditText)findViewById(R.id.etValue);
        final TextView tvResult = (TextView)findViewById(R.id.tvResultValue);
        Spinner spTo = (Spinner) findViewById(R.id.spTo);
        Spinner spFrom = (Spinner) findViewById(R.id.spFrom);
        int spToPosition = spTo.getSelectedItemPosition();
        int spFromPosition = spFrom.getSelectedItemPosition();
        if ((spToPosition >= 0) && (spFromPosition >= 0) &&
                (m_rates.getCurrencyList().size() > 0)) {
            Currency toCurrency = m_rates.getCurrencyList().get(spToPosition);
            Currency fromCurrency = m_rates.getCurrencyList().get(spFromPosition);
            String stringValue = etValue.getText().toString();
            if (!stringValue.isEmpty()) {
                float value = 0;
                boolean invalidValue = false;
                try {
                    value = Float.valueOf(stringValue);
                } catch (NumberFormatException e) {
                    invalidValue = true;
                }
                if (!invalidValue) {
                    builder.append(Float.toString(m_model.convertCurrency(toCurrency, fromCurrency, value)));
                }
            }
        }
        tvResult.setText(builder.toString());
    }

    private void restoreCachedExchangeRates() {
        setExchangeRates(m_model.getExchangeRates(this));
        if (!m_rates.getDate().isEmpty()) {
            updateInfoField(InfoType.INFO, getString(R.string.cache_valid) + m_rates.getDate());
        }
        else {
            updateInfoField(InfoType.ERROR, getString(R.string.cache_invalid));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((EditText)findViewById(R.id.etValue)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                updateExchangeResult();
            }
        });

        ((Spinner) findViewById(R.id.spTo)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateExchangeResult();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                updateExchangeResult();
            }
        });

        ((Spinner) findViewById(R.id.spFrom)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateExchangeResult();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                updateExchangeResult();
            }
        });

        restoreCachedExchangeRates();
    }

    @Override
    public void onResume() {
        super.onResume();
        m_updateObservable = m_model.updateExchangeRates(this);
        m_updateObservable.addObserver(m_updateObserver);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (m_updateObservable != null) {
            m_updateObservable.deleteObserver(m_updateObserver);
            m_updateObservable = null;
        }
        m_model.stopPendingUpdate();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }
}
