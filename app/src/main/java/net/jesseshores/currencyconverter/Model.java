package net.jesseshores.currencyconverter;

import android.content.Context;

import net.jesseshores.currencyconverter.CurrencyUtils.*;

import java.util.Observable;

class Model implements net.jesseshores.currencyconverter.contracts.Model {

    private static final String URL = "http://www.cbr.ru/scripts/XML_daily.asp";

    private final CurrencyUtils m_currencyUtils = new CurrencyUtils();

    private final StorageUtils m_storageUtils = new StorageUtils();

    private final NetworkUtils m_networkUtils = new NetworkUtils();

    private UpdateObservable m_updateObservable = null;

    private class UpdateObservable extends Observable {
        public synchronized void setChanged() {
            super.setChanged();
        }
    }

    @Override
    public float convertCurrency(Currency to, Currency from, Currency base, float value) {
        return m_currencyUtils.convertCurrency(to, from, base, value);
    }

    @Override
    public Observable retrieveExchangeRates(Context context) {
        m_updateObservable = new UpdateObservable();
        m_networkUtils.downloadFile(URL, m_storageUtils.getCacheFileName(context),
                new NetworkUtils.Callback() {
                    @Override
                    public void call(boolean result) {
                        m_updateObservable.setChanged();
                        m_updateObservable.notifyObservers(result);
                    }
                });
        return m_updateObservable;
    }

    @Override
    public void stopPendingActions() {
        m_networkUtils.stopPendingDownload();
    }
}
