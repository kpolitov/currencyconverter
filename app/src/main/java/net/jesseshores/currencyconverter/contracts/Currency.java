package net.jesseshores.currencyconverter.contracts;

public interface Currency {

    String getCharCode();

    String getName();

    float getRateToBase();
}
