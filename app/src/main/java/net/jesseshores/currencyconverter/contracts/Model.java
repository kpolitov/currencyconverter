package net.jesseshores.currencyconverter.contracts;

import android.content.Context;

import java.util.Observable;

public interface Model {
    float convertCurrency(Currency to, Currency from, float value);
    Observable retrieveExchangeRates(Context context);
    void stopPendingActions();
}
