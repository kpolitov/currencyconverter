package net.jesseshores.currencyconverter;

import android.util.Log;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class CurrencyUtils {

    private static final String TAG = CurrencyUtils.class.getSimpleName();

    @Root(name="Valute", strict=false)
    static class Currency {

        @Element
        private String CharCode;

        @Element
        private String Name;

        @Element
        private String Value;

        private float parsedValue = -1;

        @Element
        private int Nominal;

        String getCharCode() {
            return CharCode;
        }

        String getName() {
            return Name;
        }

        float getRateToBase() {
            if (parsedValue == -1) {
                NumberFormat format = NumberFormat.getInstance(Locale.FRANCE);
                Number number = 0;
                try {
                    number = format.parse(Value);
                } catch (ParseException e) {
                    Log.e(TAG, "Failed to parse float value", e);
                }
                parsedValue = number.floatValue();
            }

            return Nominal == 0 ? parsedValue : parsedValue/Nominal;
        }
/*
        public Currency() {
            this.CharCode = "";
            this.Name = "";
            this.Value = "1";
            this.Nominal = 1;
        }
*/
        Currency(String charCode, String name, String value, int nominal) {
            this.CharCode = charCode;
            this.Name = name;
            this.Value = value;
            this.Nominal = nominal;
        }
    }

    @Root(name="ValCurs")
    static class ExchangeRates {

        @Attribute
        private String Date;

        @Attribute
        private String name;

        @ElementList(inline=true)
        private List<Currency> list;

        public String getDate() {
            return Date;
        }

        public String getName() {
            return name;
        }

        public List<Currency> getCurrencyList() {
            return list;
        }

        ExchangeRates() {
            this.Date = "";
            this.name = "";
            this.list = new ArrayList<>(0);
        }

        public ExchangeRates(String date, String name) {
            this.Date = date;
            this.name = name;
        }
    }

    ExchangeRates parseCacheFile(File file) {
        Serializer serializer = new Persister();
        try {
            ExchangeRates exchangeRates = serializer.read(ExchangeRates.class, file);
            exchangeRates.getCurrencyList().add(new Currency("RUR", "Российский рубль", "1", 1));
            return exchangeRates;
        } catch (Exception e) {
            Log.e(TAG, "Unable to parse cache file", e);
            return new ExchangeRates();
        }
    }

    public float convertCurrency(Currency to, Currency from, Currency base, float value) {
        return (value*from.getRateToBase())/to.getRateToBase();
    };

}
