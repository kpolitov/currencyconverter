package net.jesseshores.currencyconverter;

import android.content.Context;

import java.io.File;

class StorageUtils {

    private static final String currencyCacheFileName = "/currencyCache";

    File readCacheFile(Context context) {
        return new File(getCacheFileName(context));
    }

    String getCacheFileName(Context context) {
        return context.getFilesDir() + currencyCacheFileName;
    }
}
