package net.jesseshores.currencyconverter;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.atomic.AtomicBoolean;

import net.jesseshores.currencyconverter.CurrencyUtils.*;

import static android.os.SystemClock.sleep;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class ModelFunctionalTest {

    private Model m_model = new Model();
    private final Context m_context = InstrumentationRegistry.getTargetContext();

    @Before
    public void setUp() {
        cleanCache();
        m_model = new Model();
    }

    private void cleanCache() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        StorageUtils m_storageUtils = new StorageUtils();
        String cacheFileName = m_storageUtils.getCacheFileName(appContext);
        File cacheFile = new File(cacheFileName);
        cacheFile.delete();
    }

    @Test
    public void getExchangeRatesReturnsEmptyClassIfCacheDoesNotExist() {
        ExchangeRates exchangeRates = m_model.getExchangeRates(m_context);
        assertNotNull(exchangeRates);
        assertEquals("", exchangeRates.getDate());
        assertEquals(0, exchangeRates.getCurrencyList().size());
    }

    @Test
    public void getExchangeRatesReturnsCorrectResultWhenCacheExists() {
        final AtomicBoolean receiveFlag = new AtomicBoolean(false);
        Observable observable = m_model.updateExchangeRates(m_context);
        observable.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                ExchangeRates exchangeRates = m_model.getExchangeRates(m_context);
                assertNotNull(exchangeRates);
                assertNotEquals("Date is empty", exchangeRates.getDate(), "");
                assertNotEquals(0, exchangeRates.getCurrencyList().size());
                receiveFlag.set(true);
            }
        });
        sleep(500);
        assertTrue(receiveFlag.get());
    }

    @Test
    public void stopPendingUpdateStopsDownload() {
        final AtomicBoolean receiveFlag = new AtomicBoolean();
        Observable observable = m_model.updateExchangeRates(m_context);
        observable.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                receiveFlag.set(true);
            }
        });
        m_model.stopPendingUpdate();
        sleep(1000);
        assertFalse(receiveFlag.get());
    }

    @Test
    public void convertCurrencyShouldCorrectlyConvertCurrencies() {
        Currency fromCurrency = new Currency("CR1", "Currency 1", "55", 1);
        Currency toCurrency = new Currency("CR2", "Currency 2", "10", 10);
        assertEquals(9.090909, m_model.convertCurrency(fromCurrency, toCurrency, 500), 0.01);
    }
}
